/*
 * 请告诉我那些没有下任何订单的顾客（customer）的信息。结果应当包含如下的信息：
 *
 * +─────────────────+───────────────+
 * | customerNumber  | customerName  |
 * +─────────────────+───────────────+
 *
 * 结果应当按照 `customerNumber` 排序。
 */


select customers.customerNumber, customers.customerName
from customers
where customers.customerNumber not in (select orders.customerNumber from orders)
order by customers.customerNumber


 /*一种不准确的实现*/
-- select customers.customerNumber, customerName
-- from customers left join orders on (customers.customerNumber = orders.customerNumber)
-- where orders.orderNumber is null or orders.orderNumber = 0
-- order by customerNumber