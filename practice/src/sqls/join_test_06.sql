/*
 * 请告诉我所有的员工（employee）的姓名及其管理者的姓名。所有的姓名都需要按照 `lastName, firstName`
 * 的格式输出，例如 'Bow, Anthony'。如果员工没有管理者，则其管理者的姓名输出为 '(Top Manager)'。
 * 输出需要包含如下信息：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */

/* 这个是if 语句
IF表达式
IF(expr1,expr2,expr3)

如果 expr1 是TRUE (expr1 <> 0 and expr1 <> NULL)，则 IF()的返回值为expr2;
否则返回值则为 expr3。IF() 的返回值为数字值或字符串值，具体情况视其所在语境而定。

select *,if(sva=1,"男","女") as ssva from taname where sva != ""*/

select concat(e.lastName, ', ', e.firstName) employee,
       if(e.reportsTo IS NULL, '(Top Manager)', concat(mana.lastName, ', ', mana.firstName)) manager

from employees e
         join employees mana on e.reportsTo = mana.employeeNumber or (e.reportsTo IS NULL and mana.reportsTo IS NULL)

order by manager, employee

