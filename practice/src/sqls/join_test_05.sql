/*
 * 请告诉我所有员工（employee）中有管理者（manager）的员工姓名及其管理者的姓名。所有的姓名
 * 请按照 `lastName, firstName` 的格式输出。例如：`Bow, Anthony`：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */

/*自连接  看看他有没有reportsTo的对象*/

SELECT CONCAT(e.lastName, ', ', e.firstName) employee,
       CONCAT(mana.lastName, ', ', mana.firstName) manager

FROM employees e JOIN employees mana ON e.reportsTo IS NOT NULL AND e.reportsTo = mana.employeeNumber

ORDER BY manager, employee