/*
 * 请告诉我订单明细（orderdetails）中明细总金额排名前五的明细记录。结果中应当包含如下信息：
 *
 * +──────────────+───────────+
 * | orderNumber  | subtotal  |
 * +──────────────+───────────+
 *
 * 结果应当按照明细总金额进行排序。
 */


select odt.orderNumber, ( odt.quantityOrdered * odt.priceEach ) subtotal
from orderdetails odt
order by subtotal desc LIMIT 5