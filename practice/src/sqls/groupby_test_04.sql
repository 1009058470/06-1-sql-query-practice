/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 * 
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */

/*having语句通常与GROUP BY语句联合使用，用来过滤由GROUP BY语句返回的记录集。

having语句的存在弥补了WHERE关键字不能与聚合函数联合使用的不足。*/

select orderNumber, sum(quantityOrdered * priceEach) totalPrice
from orderdetails
group by orderNumber
-- having 在这类似于where
having totalPrice > 60000
order by orderNumber