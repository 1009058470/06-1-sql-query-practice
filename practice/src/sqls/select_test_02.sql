/* 
 * 请告诉我 `employeeNumber` 最大的 employee 的如下信息：
 *
 * +─────────────────+────────────+───────────+
 * | employeeNumber  | firstName  | lastName  |
 * +─────────────────+────────────+───────────+
 */

-- SELECT `employeeNumber`, `firstName`, `lastName`
-- FROM `employees`
-- WHERE employeeNumber = (SELECT MAX(`employeeNumber`) FROM employees)


-- limit 没有被当成关键字
SELECT `employeeNumber`, `firstName`, `lastName`
FROM `employees`
where employeeNumber = (select distinct employeeNumber from `employees` order by employeeNumber desc limit 0,1)