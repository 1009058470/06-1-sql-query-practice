/*
 * 请告诉我顾客（customer）中所有的国家（country）的数目（不重复）。结果应当包含如下的信息：
 *
 * +────────+
 * | count  |
 * +────────+
 */

 /* 在count 不是关键字则可以去掉引号 */

 select count(distinct cs.country) 'count'
 from customers cs
